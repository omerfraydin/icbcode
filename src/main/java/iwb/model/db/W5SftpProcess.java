package iwb.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="w5_sftp_process", schema="iwb")
public class W5SftpProcess implements java.io.Serializable {
	
	//private static final long serialVersionUID = 3333124324231L;
	private int sftpProcessId;
	private String dsc;
	private int sftpDefId;
	private int sftpType;
	private String remotePath;
	private String searchDsc;
	private int objectTip;
	private int objectId;
	private String projectUuid;
	
	@SequenceGenerator(name="sex_sftp_process",sequenceName="iwb.seq_w5_sftp_process",allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sex_sftp_process")
	@Column(name="sftp_process_id")
	public int getSftpProcessId() {
		return sftpProcessId;
	}
	public void setSftpProcessId(int sftpProcessId) {
		this.sftpProcessId = sftpProcessId;
	}
	
	@Column(name="dsc")
	public String getDsc() {
		return dsc;
	}
	public void setDsc(String dsc) {
		this.dsc = dsc;
	}
	
	@Column(name="sftp_def_id")
	public int getSftpDefId() {
		return sftpDefId;
	}
	public void setSftpDefId(int sftpDefId) {
		this.sftpDefId = sftpDefId;
	}
	
	@Column(name="sftp_type")
	public int getSftpType() {
		return sftpType;
	}
	public void setSftpType(int sftpType) {
		this.sftpType = sftpType;
	}
	
	@Column(name="remote_path")
	public String getRemotePath() {
		return remotePath;
	}
	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
	
	@Column(name="search_dsc")
	public String getSearchDsc() {
		return searchDsc;
	}
	public void setSearchDsc(String searchDsc) {
		this.searchDsc = searchDsc;
	}
	
	@Column(name="object_tip")
	public int getObjectTip() {
		return objectTip;
	}
	public void setObjectTip(int objectTip) {
		this.objectTip = objectTip;
	}
	@Column(name="object_id")
	public int getObjectId() {
		return objectId;
	}
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}
	
	@Column(name="project_uuid")
	public String getProjectUuid() {
		return projectUuid;
	}
	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	
	

}
