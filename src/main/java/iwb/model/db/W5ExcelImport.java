package iwb.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="w5_excel_import", schema="iwb")
public class W5ExcelImport implements java.io.Serializable {
/*TABLE_ID: 666*/

	private static final long serialVersionUID = 3333124324231L;
	private int excelImportId;	
	private String dsc;
	private String systemFileName;
	private String projectUuid;
	private int insertUserId;
	private int tableId;
	private int gridId;
	private int importType;
	private int dbFuncId;
	private int startRowNo;
	private String templateFileName;
	private int customizationId;
	private int activeFlag;
	private int tabOrder;
	private int row_error_strategy_tip;
	
	
	@SequenceGenerator(name="sex_excel_import",sequenceName="iwb.seq_excel_import",allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sex_excel_import")
	@Column(name="excel_import_id")
	public int getExcelImportId() {
		return excelImportId;
	}
	public void setExcelImportId(int excelImportId) {
		this.excelImportId = excelImportId;
	}


	@Column(name="dsc")
	public String getDsc() {
		return dsc;
	}
	public void setDsc(String dsc) {
		this.dsc = dsc;
	}	
	
	
	@Column(name="insert_user_id")
	public int getInsertUserId() {
		return insertUserId;
	}
	public void setInsertUserId(int insertUserId) {
		this.insertUserId = insertUserId;
	}
	
	
	@Column(name="system_file_name")
	public String getSystemFileName() {
		return systemFileName;
	}
	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}
	
	
	@Transient
	public boolean safeEquals(W5Base q){
		return false;
	}
	

	@Column(name="project_uuid")
	public String getProjectUuid() {
		return projectUuid;
	}

	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	
	@Column(name="table_id")
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	
	@Column(name="grid_id")
	public int getGridId() {
		return gridId;
	}
	public void setGridId(int gridId) {
		this.gridId = gridId;
	}
	
	@Column(name="import_type")
	public int getImportType() {
		return importType;
	}
	public void setImportType(int importType) {
		this.importType = importType;
	}
	
	@Column(name="db_func_id")
	public int getDbFuncId() {
		return dbFuncId;
	}
	public void setDbFuncId(int dbFuncId) {
		this.dbFuncId = dbFuncId;
	}
	
	@Column(name="start_row_no")
	public int getStartRowNo() {
		return startRowNo;
	}
	public void setStartRowNo(int startRowNo) {
		this.startRowNo = startRowNo;
	}
	
	@Column(name="template_file_name")
	public String getTemplateFileName() {
		return templateFileName;
	}
	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}
	
	@Column(name="customization_id")
	public int getCustomizationId() {
		return customizationId;
	}
	public void setCustomizationId(int customizationId) {
		this.customizationId = customizationId;
	}
	
	@Column(name="active_flag")
	public int getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}
	
	@Column(name="tab_order")
	public int getTabOrder() {
		return tabOrder;
	}
	public void setTabOrder(int tabOrder) {
		this.tabOrder = tabOrder;
	}

}
