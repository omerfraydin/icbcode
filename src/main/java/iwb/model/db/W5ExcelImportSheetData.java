package iwb.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="w5_excel_import_sheet_data",schema="iwb")
public class W5ExcelImportSheetData implements java.io.Serializable {
/*TABLE_ID: 669*/

	private static final long serialVersionUID = 76762342342341L;
	private int excelImportSheetDataId;
	private int excelImportSheetId;
	private int rowNo;
	private String projectUuid;
	private int customizationId;
	private int excelImportId;
	private int fileImportId;
	private int sheetNo;
	private short importFlag;
	
	private String a;
	private String b;
	private String c;
	private String d;
	private String e;
	private String f;
	private String g;
	private String h;
	private String i;
	private String j;
	private String k;
	private String l;
	private String m;
	private String n;
	private String o;
	private String p;
	private String q;
	private String r;
	private String s;
	private String t;
	private String u;
	private String v;	
	private String w;
	private String x;
	private String y;
	private String z;

	private String a2;
	private String b2;
	private String c2;
	private String d2;
	private String e2;
	private String f2;
	private String g2;
	private String h2;
	private String i2;
	private String j2;
	private String k2;
	private String l2;
	private String m2;
	private String n2;
	private String o2;
	private String p2;
	private String q2;
	private String r2;
	private String s2;
	private String t2;
	private String u2;
	private String v2;	
	private String w2;
	private String x2;
	private String y2;
	private String z2;
	
	private String a3;
	private String b3;
	private String c3;
	private String d3;
	private String e3;
	private String f3;
	private String g3;
	private String h3;
	private String i3;
	private String j3;
	private String k3;
	private String l3;
	private String m3;
	private String n3;
	private String o3;
	private String p3;
	private String q3;
	private String r3;
	private String s3;
	private String t3;
	private String u3;
	private String v3;	
	private String w3;
	private String x3;
	private String y3;
	private String z3;
	
	
	@SequenceGenerator(name="sex_excel_import_sheet_data",sequenceName="iwb.seq_excel_import_sheet_data",allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sex_excel_import_sheet_data")
	@Column(name="excel_import_sheet_data_id")
	public int getExcelImportSheetDataId() {
		return excelImportSheetDataId;
	}



	public void setExcelImportSheetDataId(int excelImportSheetDataId) {
		this.excelImportSheetDataId = excelImportSheetDataId;
	}



	@Column(name="excel_import_sheet_id")
	public int getExcelImportSheetId() {
		return excelImportSheetId;
	}



	public void setExcelImportSheetId(int excelImportSheetId) {
		this.excelImportSheetId = excelImportSheetId;
	}


	@Column(name="import_flag")
	public short getImportFlag() {
		return importFlag;
	}



	public void setImportFlag(short importFlag) {
		this.importFlag = importFlag;
	}



	public void setCell(String column, String val){
		switch(column){
		case "A":a=val;break;
		case "B":b=val;break;
		case "C":c=val;break;
		case "D":d=val;break;
		case "E":e=val;break;
		case "F":f=val;break;
		case "G":g=val;break;
		case "H":h=val;break;
		case "I":i=val;break;
		case "J":j=val;break;
		case "K":k=val;break;
		case "L":l=val;break;
		case "M":m=val;break;
		case "N":n=val;break;
		case "O":o=val;break;
		case "P":p=val;break;
		case "Q":q=val;break;
		case "R":r=val;break;
		case "S":s=val;break;
		case "T":t=val;break;
		case "U":u=val;break;
		case "V":v=val;break;
		case "W":w=val;break;
		case "X":x=val;break;
		case "Y":y=val;break;
		case "Z":z=val;break;
		case "AA":a2=val;break;
		case "AB":b2=val;break;
		case "AC":c2=val;break;
		case "AD":d2=val;break;
		case "AE":e2=val;break;
		case "AF":f2=val;break;
		case "AG":g2=val;break;
		case "AH":h2=val;break;
		case "AI":i2=val;break;
		case "AJ":j2=val;break;
		case "AK":k2=val;break;
		case "AL":l2=val;break;
		case "AM":m2=val;break;
		case "AN":n2=val;break;
		case "AO":o2=val;break;
		case "AP":p2=val;break;
		case "AQ":q2=val;break;
		case "AR":r2=val;break;
		case "AS":s2=val;break;
		case "AT":t2=val;break;
		case "AU":u2=val;break;
		case "AV":v2=val;break;
		case "AW":w2=val;break;
		case "AX":x2=val;break;
		case "AY":y2=val;break;
		case "AZ":z2=val;break;
		case "BA":a3=val;break;
		case "BB":b3=val;break;
		case "BC":c3=val;break;
		case "BD":d3=val;break;
		case "BE":e3=val;break;
		case "BF":f3=val;break;
		case "BG":g3=val;break;
		case "BH":h3=val;break;
		case "BI":i3=val;break;
		case "BJ":j3=val;break;
		case "BK":k3=val;break;
		case "BL":l3=val;break;
		case "BM":m3=val;break;
		case "BN":n3=val;break;
		case "BO":o3=val;break;
		case "BP":p3=val;break;
		case "BQ":q3=val;break;
		case "BR":r3=val;break;
		case "BS":s3=val;break;
		case "BT":t3=val;break;
		case "BU":u3=val;break;
		case "BV":v3=val;break;
		case "BW":w3=val;break;
		case "BX":x3=val;break;
		case "BY":y3=val;break;
		case "BZ":z3=val;break;		
		}
	}

	
		
	@Column(name="row_no")
	public int getRowNo() {
		return rowNo;
	}

	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}

	
	@Column(name="a")
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	@Column(name="b")
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	@Column(name="c")
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	@Column(name="d")
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	@Column(name="e")
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	@Column(name="f")
	public String getF() {
		return f;
	}
	public void setF(String f) {
		this.f = f;
	}
	@Column(name="g")
	public String getG() {
		return g;
	}
	public void setG(String g) {
		this.g = g;
	}
	@Column(name="h")
	public String getH() {
		return h;
	}
	public void setH(String h) {
		this.h = h;
	}
	@Column(name="i")
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	@Column(name="j")
	public String getJ() {
		return j;
	}
	public void setJ(String j) {
		this.j = j;
	}
	@Column(name="k")
	public String getK() {
		return k;
	}
	public void setK(String k) {
		this.k = k;
	}
	@Column(name="l")
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	@Column(name="m")
	public String getM() {
		return m;
	}
	public void setM(String m) {
		this.m = m;
	}
	@Column(name="n")
	public String getN() {
		return n;
	}
	public void setN(String n) {
		this.n = n;
	}
	@Column(name="o")
	public String getO() {
		return o;
	}
	public void setO(String o) {
		this.o = o;
	}	
	@Column(name="p")
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	@Column(name="q")
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	@Column(name="r")
	public String getR() {
		return r;
	}
	public void setR(String r) {
		this.r = r;
	}
	@Column(name="s")
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	@Column(name="t")
	public String getT() {
		return t;
	}
	public void setT(String t) {
		this.t = t;
	}
	@Column(name="u")
	public String getU() {
		return u;
	}
	public void setU(String u) {
		this.u = u;
	}
	@Column(name="v")
	public String getV() {
		return v;
	}
	public void setV(String v) {
		this.v = v;
	}
	@Column(name="w")
	public String getW() {
		return w;
	}
	public void setW(String w) {
		this.w = w;
	}
	@Column(name="x")
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	@Column(name="y")
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	@Column(name="z")
	public String getZ() {
		return z;
	}
	public void setZ(String z) {
		this.z = z;
	}



	public String getA2() {
		return a2;
	}



	public void setA2(String a2) {
		this.a2 = a2;
	}



	public String getB2() {
		return b2;
	}



	public void setB2(String b2) {
		this.b2 = b2;
	}



	public String getC2() {
		return c2;
	}



	public void setC2(String c2) {
		this.c2 = c2;
	}



	public String getD2() {
		return d2;
	}



	public void setD2(String d2) {
		this.d2 = d2;
	}



	public String getE2() {
		return e2;
	}



	public void setE2(String e2) {
		this.e2 = e2;
	}



	public String getF2() {
		return f2;
	}



	public void setF2(String f2) {
		this.f2 = f2;
	}



	public String getG2() {
		return g2;
	}



	public void setG2(String g2) {
		this.g2 = g2;
	}



	public String getH2() {
		return h2;
	}



	public void setH2(String h2) {
		this.h2 = h2;
	}



	public String getI2() {
		return i2;
	}



	public void setI2(String i2) {
		this.i2 = i2;
	}



	public String getJ2() {
		return j2;
	}



	public void setJ2(String j2) {
		this.j2 = j2;
	}



	public String getK2() {
		return k2;
	}



	public void setK2(String k2) {
		this.k2 = k2;
	}



	public String getL2() {
		return l2;
	}



	public void setL2(String l2) {
		this.l2 = l2;
	}



	public String getM2() {
		return m2;
	}



	public void setM2(String m2) {
		this.m2 = m2;
	}



	public String getN2() {
		return n2;
	}



	public void setN2(String n2) {
		this.n2 = n2;
	}



	public String getO2() {
		return o2;
	}



	public void setO2(String o2) {
		this.o2 = o2;
	}



	public String getP2() {
		return p2;
	}



	public void setP2(String p2) {
		this.p2 = p2;
	}



	public String getQ2() {
		return q2;
	}



	public void setQ2(String q2) {
		this.q2 = q2;
	}



	public String getR2() {
		return r2;
	}



	public void setR2(String r2) {
		this.r2 = r2;
	}



	public String getS2() {
		return s2;
	}



	public void setS2(String s2) {
		this.s2 = s2;
	}



	public String getT2() {
		return t2;
	}



	public void setT2(String t2) {
		this.t2 = t2;
	}



	public String getU2() {
		return u2;
	}



	public void setU2(String u2) {
		this.u2 = u2;
	}



	public String getV2() {
		return v2;
	}



	public void setV2(String v2) {
		this.v2 = v2;
	}



	public String getW2() {
		return w2;
	}



	public void setW2(String w2) {
		this.w2 = w2;
	}



	public String getX2() {
		return x2;
	}



	public void setX2(String x2) {
		this.x2 = x2;
	}



	public String getY2() {
		return y2;
	}



	public void setY2(String y2) {
		this.y2 = y2;
	}



	public String getZ2() {
		return z2;
	}



	public void setZ2(String z2) {
		this.z2 = z2;
	}

	
	

	@Column(name="project_uuid")
	public String getProjectUuid() {
		return projectUuid;
	}

	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}


	@Column(name="customization_id")
	public int getCustomizationId() {
		return customizationId;
	}



	public void setCustomizationId(int customizationId) {
		this.customizationId = customizationId;
	}


	@Column(name="excel_import_id")
	public int getExcelImportId() {
		return excelImportId;
	}


	
	public void setExcelImportId(int excelImportId) {
		this.excelImportId = excelImportId;
	}


	@Column(name="file_import_id")
	public int getFileImportId() {
		return fileImportId;
	}


	
	public void setFileImportId(int fileImportId) {
		this.fileImportId = fileImportId;
	}


	@Column(name="a3")
	public String getA3() {
		return a3;
	}



	public void setA3(String a3) {
		this.a3 = a3;
	}


	@Column(name="b3")
	public String getB3() {
		return b3;
	}



	public void setB3(String b3) {
		this.b3 = b3;
	}


	@Column(name="c3")
	public String getC3() {
		return c3;
	}



	public void setC3(String c3) {
		this.c3 = c3;
	}


	@Column(name="d3")
	public String getD3() {
		return d3;
	}



	public void setD3(String d3) {
		this.d3 = d3;
	}


	@Column(name="e3")
	public String getE3() {
		return e3;
	}



	public void setE3(String e3) {
		this.e3 = e3;
	}


	@Column(name="f3")
	public String getF3() {
		return f3;
	}



	public void setF3(String f3) {
		this.f3 = f3;
	}


	@Column(name="g3")
	public String getG3() {
		return g3;
	}



	public void setG3(String g3) {
		this.g3 = g3;
	}


	@Column(name="h3")
	public String getH3() {
		return h3;
	}



	public void setH3(String h3) {
		this.h3 = h3;
	}


	@Column(name="i3")
	public String getI3() {
		return i3;
	}



	public void setI3(String i3) {
		this.i3 = i3;
	}


	@Column(name="j3")
	public String getJ3() {
		return j3;
	}



	public void setJ3(String j3) {
		this.j3 = j3;
	}


	@Column(name="k3")
	public String getK3() {
		return k3;
	}



	public void setK3(String k3) {
		this.k3 = k3;
	}


	@Column(name="l3")
	public String getL3() {
		return l3;
	}



	public void setL3(String l3) {
		this.l3 = l3;
	}


	@Column(name="m3")
	public String getM3() {
		return m3;
	}



	public void setM3(String m3) {
		this.m3 = m3;
	}


	@Column(name="n3")
	public String getN3() {
		return n3;
	}



	public void setN3(String n3) {
		this.n3 = n3;
	}


	@Column(name="o3")
	public String getO3() {
		return o3;
	}



	public void setO3(String o3) {
		this.o3 = o3;
	}


	@Column(name="p3")
	public String getP3() {
		return p3;
	}



	public void setP3(String p3) {
		this.p3 = p3;
	}


	@Column(name="q3")
	public String getQ3() {
		return q3;
	}



	public void setQ3(String q3) {
		this.q3 = q3;
	}


	@Column(name="r3")
	public String getR3() {
		return r3;
	}


	
	public void setR3(String r3) {
		this.r3 = r3;
	}


	@Column(name="s3")
	public String getS3() {
		return s3;
	}



	public void setS3(String s3) {
		this.s3 = s3;
	}


	@Column(name="t3")
	public String getT3() {
		return t3;
	}



	public void setT3(String t3) {
		this.t3 = t3;
	}


	@Column(name="u3")
	public String getU3() {
		return u3;
	}



	public void setU3(String u3) {
		this.u3 = u3;
	}


	@Column(name="v3")
	public String getV3() {
		return v3;
	}



	public void setV3(String v3) {
		this.v3 = v3;
	}


	@Column(name="w3")
	public String getW3() {
		return w3;
	}



	public void setW3(String w3) {
		this.w3 = w3;
	}


	@Column(name="x3")
	public String getX3() {
		return x3;
	}



	public void setX3(String x3) {
		this.x3 = x3;
	}


	@Column(name="y3")
	public String getY3() {
		return y3;
	}



	public void setY3(String y3) {
		this.y3 = y3;
	}


	@Column(name="z3")
	public String getZ3() {
		return z3;
	}



	public void setZ3(String z3) {
		this.z3 = z3;
	}


	@Column(name="sheet_no")
	public int getSheetNo() {
		return sheetNo;
	}



	public void setSheetNo(int sheetNo) {
		this.sheetNo = sheetNo;
	}
	
	
}
