package iwb.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="w5_file_import", schema="iwb")
public class W5FileImport implements java.io.Serializable {
	
	private int fileImportId;
	private int excelImportId;
	private int customizationId;
	private int tableId;
	private String dsc;
	private int masterTablePk;
	private int objectTip;
	private int objectId;
	private int fileImportTip;
	private int row_error_strategy_tip;
	private int insertUserId;
	private int versionUserId;
	private Timestamp insertDttm;
	private Timestamp versionDttm;
	
	private String projectUuid;
	
	@SequenceGenerator(name="sex_file_import",sequenceName="iwb.seq_w5_file_import",allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sex_file_import")
	@Column(name="file_import_id")
	public int getFileImportId() {
		return fileImportId;
	}
	public void setFileImportId(int fileImportId) {
		this.fileImportId = fileImportId;
	}
	
	@Column(name="excel_import_id")
	public int getExcelImportId() {
		return excelImportId;
	}
	public void setExcelImportId(int excelImportId) {
		this.excelImportId = excelImportId;
	}
	
	@Column(name="customization_id")
	public int getCustomizationId() {
		return customizationId;
	}
	public void setCustomizationId(int customizationId) {
		this.customizationId = customizationId;
	}
	
	@Column(name="table_id")
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	
	@Column(name="dsc")
	public String getDsc() {
		return dsc;
	}
	public void setDsc(String dsc) {
		this.dsc = dsc;
	}
	
	@Column(name="master_table_pk")
	public int getMasterTablePk() {
		return masterTablePk;
	}
	public void setMasterTablePk(int masterTablePk) {
		this.masterTablePk = masterTablePk;
	}
	
	@Column(name="object_tip")
	public int getObjectTip() {
		return objectTip;
	}
	public void setObjectTip(int objectTip) {
		this.objectTip = objectTip;
	}
	
	@Column(name="object_id")
	public int getObjectId() {
		return objectId;
	}
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}
	
	@Column(name="file_import_tip")
	public int getFileImportTip() {
		return fileImportTip;
	}
	public void setFileImportTip(int fileImportTip) {
		this.fileImportTip = fileImportTip;
	}
	
	@Column(name="row_error_strategy_tip")
	public int getRow_error_strategy_tip() {
		return row_error_strategy_tip;
	}
	public void setRow_error_strategy_tip(int row_error_strategy_tip) {
		this.row_error_strategy_tip = row_error_strategy_tip;
	}
	
	@Column(name="project_uuid")
	public String getProjectUuid() {
		return projectUuid;
	}
	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	
	@Column(name="insert_user_id")
	public int getInsertUserId() {
		return insertUserId;
	}
	public void setInsertUserId(int insertUserId) {
		this.insertUserId = insertUserId;
	}
	
	@Column(name="version_user_id")
	public int getVersionUserId() {
		return versionUserId;
	}
	public void setVersionUserId(int versionUserId) {
		this.versionUserId = versionUserId;
	}
	
	@Column(name="insert_dttm")
	public Timestamp getInsertDttm() {
		return insertDttm;
	}
	public void setInsertDttm(Timestamp insertDttm) {
		this.insertDttm = insertDttm;
	}
	
	@Column(name="version_dttm")
	public Timestamp getVersionDttm() {
		return versionDttm;
	}
	public void setVersionDttm(Timestamp versionDttm) {
		this.versionDttm = versionDttm;
	}
	
	
	

	
	
	
	
}
