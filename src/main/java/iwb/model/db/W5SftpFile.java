package iwb.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="w5_sftp_file", schema="iwb")
public class W5SftpFile implements java.io.Serializable{
	
	//private static final long serialVersionUID = 3333124324231L;
	private int sftpFileId;
	private int sftpProcessId;
	private String fileName;
	private int fileType;
	private int size;
	private long atime;
	private long mtime;
	private String projectUuid;
	private short processFlag;
	
	@SequenceGenerator(name="sex_sftp_file",sequenceName="iwb.seq_w5_sftp_file",allocationSize=1)
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sex_sftp_file")
	@Column(name="sftp_file_id")
	public int getSftpFileId() {
		return sftpFileId;
	}
	public void setSftpFileId(int sftpFileId) {
		this.sftpFileId = sftpFileId;
	}
	
	@Column(name="sftp_process_id")
	public int getSftpProcessId() {
		return sftpProcessId;
	}
	public void setSftpProcessId(int sftpProcessId) {
		this.sftpProcessId = sftpProcessId;
	}
	
	@Column(name="file_name")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name="file_type")
	public int getFileType() {
		return fileType;
	}
	public void setFileType(int fileType) {
		this.fileType = fileType;
	}
	
	@Column(name="size")
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	@Column(name="atime")
	public long getAtime() {
		return atime;
	}
	public void setAtime(long atime) {
		this.atime = atime;
	}
	
	@Column(name="mtime")
	public long getMtime() {
		return mtime;
	}
	public void setMtime(long mtime) {
		this.mtime = mtime;
	}
	
	@Column(name="project_uuid")
	public String getProjectUuid() {
		return projectUuid;
	}
	public void setProjectUuid(String projectUuid) {
		this.projectUuid = projectUuid;
	}
	
	@Column(name="process_flag")
	public short getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(short processFlag) {
		this.processFlag = processFlag;
	}
	
	
	
}
