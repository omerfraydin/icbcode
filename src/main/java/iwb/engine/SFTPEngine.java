package iwb.engine;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import iwb.dao.metadata.MetadataLoader;
import iwb.dao.rdbms_impl.PostgreSQL;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

@Component
public class SFTPEngine {

	@Lazy
	@Autowired
	private PostgreSQL dao;
	
	@Lazy
	@Autowired
	private MetadataLoader metadataLoader;
	
	@Lazy
	@Autowired
	private QueryEngine queryEngine;
	
	private SSHClient setupSshj(String remoteHost,int port, String userName, String passWord) throws IOException {
	    SSHClient client = new SSHClient();
	    client.addHostKeyVerifier(new PromiscuousVerifier());
	    client.connect(remoteHost,port);
	    client.authPassword(userName, passWord);
	    return client;
	}
	
}
