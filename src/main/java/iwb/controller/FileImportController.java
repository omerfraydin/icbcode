package iwb.controller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import iwb.adapter.ui.ViewAdapter;
import iwb.adapter.ui.ViewMobileAdapter;
import iwb.adapter.ui.extjs.ExtJs3_4;
import iwb.adapter.ui.f7.F7_4;
import iwb.adapter.ui.react.GReact16;
import iwb.adapter.ui.react.PrimeReact16;
import iwb.adapter.ui.react.React16;
import iwb.adapter.ui.vue.Vue2;
import iwb.adapter.ui.webix.Webix3_3;
import iwb.cache.FrameworkCache;
import iwb.cache.FrameworkSetting;
import iwb.model.db.W5ExcelImport;
import iwb.model.result.W5GlobalFuncResult;
import iwb.service.FrameworkService;
import iwb.service.ImportService;
import iwb.util.GenericUtil;
import iwb.util.UserUtil;

@Controller
@RequestMapping("/fl")
public class FileImportController implements InitializingBean {
	private static Logger logger = Logger.getLogger(AppController.class);
	
	private ViewAdapter ext3_4;
	private	ViewAdapter	webix3_3;
	private	ViewAdapter	react16;
	private	ViewAdapter	preact16;
	private	ViewAdapter	greact16;
	private	ViewAdapter	vue2;
	private ViewMobileAdapter f7;

	@Autowired
	private FrameworkService service;
	
	@Autowired
	private ImportService importService;

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		ext3_4 = new ExtJs3_4();
		webix3_3 = new Webix3_3();
		f7 = new F7_4();
		react16 = new React16();
		preact16 = new PrimeReact16();
		greact16 = new GReact16();
		vue2 = new Vue2();
		
	}
	
	
	private ViewAdapter getViewAdapter(Map<String, Object> scd, HttpServletRequest request, ViewAdapter defaultRenderer){
		if(GenericUtil.uInt(scd.get("mobile"))!=0)return ext3_4;
		if(request!=null){
			String renderer = request.getParameter("_renderer");
			if(renderer!=null && renderer.equals("ext3_4"))return ext3_4;
			if(renderer!=null && renderer.startsWith("webix"))return webix3_3;
			if(renderer!=null && renderer.equals("react16"))return react16;
			if(renderer!=null && renderer.equals("greact16"))return greact16;
			if(renderer!=null && renderer.equals("preact16"))return preact16;
			if(renderer!=null && renderer.equals("vue2"))return vue2;
		}
		if(scd!=null){
			String renderer = (String)scd.get("_renderer");
			if(renderer!=null && renderer.equals("ext3_4"))return ext3_4;
			if(renderer!=null && renderer.startsWith("webix"))return webix3_3;			
			if(renderer!=null && renderer.equals("react16"))return react16;
			if(renderer!=null && renderer.equals("greact16"))return greact16;
			if(renderer!=null && renderer.equals("preact16"))return preact16;
			if(renderer!=null && renderer.equals("vue2"))return vue2;
		}
		return defaultRenderer;
	}
	
	private ViewAdapter getViewAdapter(Map<String, Object> scd, HttpServletRequest request){
		return getViewAdapter(scd, request, ext3_4);
	}
	
	@RequestMapping(value ="/fileimport")
	@ResponseBody
	public String fileImportHandler(
		@RequestParam("file") MultipartFile file,
		@RequestParam("excel_import_id") Integer excelImportId,
		HttpServletRequest request,
		HttpServletResponse response
	)throws IOException{
		logger.info("fileImportHandler");	
		Map<String, String> reqParams=GenericUtil.getParameterMap(request);
		String projectId =reqParams.get("project_uuid");
		if(projectId.equals("scd-dev")== false) {
			projectId="preview-"+projectId;
		}
		Map<String, Object> scd = UserUtil.getScd(request,projectId,false);
	    int file_import_id = 0;
		try {
			if(!file.isEmpty()){
				String path = FrameworkCache.getAppSettingStringValue(0, "file_local_path") + File.separator
						+ scd.get("customizationId") + File.separator + "attachment";

				File dirPath = new File(path);
				if (!dirPath.exists()) {
					if (!dirPath.mkdirs())
						return "{ \"success\":false, \"msg\":\"wrong file path: " + path + "\"}";
				}
			    // f.transferTo(new File(path + File.separator + fa.getSystemFileName()));
			    String uploadedFilePath = path + File.separator + GenericUtil.strUTF2En(file.getOriginalFilename());
			    String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1).toLowerCase();
			    File tmpFile = new File(uploadedFilePath);
			    file.transferTo(tmpFile);
			    if(extension.compareTo("xls") == 0 || extension.compareTo("xlsx") == 0){
			    	file_import_id = importService.saveFileImport(scd,tmpFile, excelImportId);			    	
			    }else if(extension.compareTo("csv") == 0){
			    	
			    }
			    
			    tmpFile.delete();
			}
			return "{ \"success\": true , \"file_import_id\": "+file_import_id+", \"excel_import_id\":"+excelImportId+"}";
		} catch (Exception e){
			if(true || FrameworkSetting.debug)e.printStackTrace();
			return "{ \"success\": false }";
		}
	}
	
	
	@RequestMapping("/importDataTable")
	public void hndImportExcelTable(
			HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("hndImportExcelTable"); 
		Map<String, String> reqParams=GenericUtil.getParameterMap(request);
		String projectId =reqParams.get("project_uuid");
		int devFlag =GenericUtil.uInt(reqParams.get("scdFlag"));
		if(devFlag==1) {
			projectId="scd-dev";
		}else {
			if(projectId.equals("scd-dev")== false) {
				projectId="preview-"+projectId;
			}	
		}

		Map<String, Object> scd = UserUtil.getScd(request,projectId,false);
    	
    	response.setContentType("application/json");
    	
    	Integer excelImportId=GenericUtil.uInteger(reqParams.get("excel_import_id")); 
    	Integer fileImportId=GenericUtil.uInteger(reqParams.get("file_import_id"));
    	Integer fileImportLimit=(GenericUtil.uInteger(reqParams.get("import_limit")) != null ? GenericUtil.uInteger(reqParams.get("import_limit")) : 1000);
    	
    	W5ExcelImport exImp=service.getW5ExcelImport(scd, excelImportId);
    			
    	if (exImp!=null){
				//db function Ã§alÄ±ÅŸtÄ±rÄ±lacak		
				int dbFuncId= exImp.getDbFuncId();
				if(dbFuncId!=0){
					reqParams.put("pexcel_import_id", String.valueOf(excelImportId));
					reqParams.put("pfile_import_id", String.valueOf(fileImportId));
					//reqParams.put("pimport_limit", String.valueOf(fileImportLimit));
					W5GlobalFuncResult dbFuncResult = service.executeFunc(scd, dbFuncId, reqParams,(short)1);	
					
					response.setContentType("application/json");
					response.getWriter().write(getViewAdapter(scd, request).serializeGlobalFunc(dbFuncResult).toString());
					response.getWriter().close();
				}			
    	}else{
    		response.getWriter().write("{ \"success\": false }");
    	}
    	response.getWriter().close();
	}
}	



