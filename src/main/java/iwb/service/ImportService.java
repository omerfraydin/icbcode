package iwb.service;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import iwb.cache.FrameworkCache;
import iwb.dao.rdbms_impl.PostgreSQL;
import iwb.model.db.W5FileImport;

import iwb.engine.GlobalScriptEngine;
import iwb.model.db.W5ExcelImport;
import iwb.model.db.W5ExcelImportSheet;
import iwb.model.db.W5ExcelImportSheetData;

import iwb.util.ExcelUtil;
import iwb.util.GenericUtil;

@Service
@Transactional
public class ImportService {
	@Lazy
	@Autowired
	private PostgreSQL dao;
	
	@Lazy
	@Autowired
	private GlobalScriptEngine scriptEngine;
	
	public int saveExcelImport(Map<String, Object> scd, String fileName, String systemFileName, LinkedHashMap<String, List<HashMap<String, String>>> parsedData) {
    	W5FileImport im = new W5FileImport();
    	im.setCustomizationId(GenericUtil.uInt(scd.get("customizationId")));
    	im.setDsc(fileName);
    	im.setTableId(GenericUtil.uInt(666));
    	im.setExcelImportId(0);
    	long time = System.currentTimeMillis();
    	im.setInsertDttm(new Timestamp(time));
    	im.setVersionDttm(new Timestamp(time));
    	im.setInsertUserId(GenericUtil.uInt(scd.get("userId")));
    	im.setVersionUserId(GenericUtil.uInt(scd.get("userId")));
    	im.setRow_error_strategy_tip(GenericUtil.uInt(0));
    	im.setObjectTip(GenericUtil.uInt(0));
    	im.setObjectId(GenericUtil.uInt(0));
    	im.setMasterTablePk(GenericUtil.uInt(0));
    	im.setProjectUuid((String)scd.get("projectId"));
    	short sheetNo = 1;
    	for(Entry<String, List<HashMap<String, String>>> sheet : parsedData.entrySet())if(sheet.getValue()!=null && sheet.getValue().size()>0){
        	W5ExcelImportSheet ims = new W5ExcelImportSheet();
        	ims.setProjectUuid(im.getProjectUuid());
        	ims.setDsc(sheet.getKey());
        	ims.setTabOrder(sheetNo);
        	ims.setExcelImportId(im.getExcelImportId());
        	ims.setFileImportId(im.getFileImportId());
        	ims.setCustomizationId(im.getCustomizationId());
        	dao.saveObject(ims); 	
        	
        	List<Object> toBeSaved = new ArrayList();

    		for(int i=0; i<sheet.getValue().size(); i++){
	    		W5ExcelImportSheetData imd = new W5ExcelImportSheetData();
	    		imd.setRowNo(i+1);
	    		imd.setExcelImportSheetId(ims.getExcelImportSheetId());
	    		imd.setProjectUuid(im.getProjectUuid());
	    		imd.setFileImportId(im.getFileImportId());
	    		imd.setSheetNo(sheetNo);
	    		imd.setExcelImportId(im.getExcelImportId());
	    		imd.setImportFlag((short)0);
	    		imd.setCustomizationId(im.getCustomizationId());	    		
	    		for(Entry<String, String> entryCols : sheet.getValue().get(i).entrySet()){
	    			imd.setCell(entryCols.getKey(),entryCols.getValue());	
	    		}
	    		toBeSaved.add(imd);
    		}
    		sheetNo++;
    		if(toBeSaved.size()>0)for(Object o:toBeSaved) dao.saveObject(o);
    	}

    	return im.getExcelImportId();
		
		
	}


	public void importAppMaker(Map<String, Object> scd, String name, String body, String script) {
		Map m = new HashMap();
		m.put("pbody", body);
		if(name.indexOf('/')>-1) {
			m.put("pname", name.substring(name.indexOf('/')+1));
			String dir  = name.substring(0,name.indexOf('/'));
			if(dir.equals("models")) {
				scriptEngine.executeGlobalFunc(scd, 467, m, (short) 1);
			} else if(dir.equals("pages")) {
				scriptEngine.executeGlobalFunc(scd, 621, m, (short) 1);
			} else if(dir.equals("relations")) {
				scriptEngine.executeGlobalFunc(scd, 5, m, (short) 1);
			} else if(dir.equals("scripts")) {
				m.put("pscript", script);
				scriptEngine.executeGlobalFunc(scd, 269, m, (short) 1);
			}
		} else {
			m.put("pname", name);
			scriptEngine.executeGlobalFunc(scd, 1204, m, (short) 1);
		}
			
		
	}
	
	public int saveFileImport(Map<String, Object> scd, File tmpFile, Integer excelImportId) {
		int file_import_id = 0;
    	//// VeritabanÄ±na kaydetme bitti 
		ExcelUtil p = new ExcelUtil(tmpFile.getPath());
    	// ilk veri hangi sheet olduÄŸu, sonraki hangi satÄ±r olduÄŸu
    	LinkedHashMap<String,List<HashMap<String,String>>> parsedData = p.parseExcel();
    	
    	W5ExcelImport ei =null;
		if(FrameworkCache.wExcelImport!=null && FrameworkCache.wExcelImport.get((String)scd.get("projectId"))!=null){
			for (W5ExcelImport imp : FrameworkCache.wExcelImport.get((String)scd.get("projectId"))){
				if (imp.getExcelImportId()==excelImportId){
					ei=imp;
					break;
				}
			}
		}
    	
    	if(parsedData != null && parsedData.size() > 0){
    		/* file import ediliyor.*/
	    	W5FileImport im = new W5FileImport();
	    	im.setCustomizationId(GenericUtil.uInt(scd.get("customizationId")));
	    	im.setDsc(tmpFile.getName());
	    	im.setTableId(ei.getTableId());
	    	if (excelImportId!=null)im.setExcelImportId(excelImportId);
	    	long time = System.currentTimeMillis();
	    	im.setInsertDttm(new Timestamp(time));
	    	im.setVersionDttm(new Timestamp(time));
	    	im.setInsertUserId(GenericUtil.uInt(scd.get("userId")));
	    	im.setVersionUserId(GenericUtil.uInt(scd.get("userId")));
	    	im.setRow_error_strategy_tip(0);
	    	im.setObjectTip(0);
	    	im.setObjectId(0);
	    	im.setMasterTablePk(0);
	    	im.setProjectUuid((String)scd.get("projectId"));
	    	dao.saveObject(im);
	    	file_import_id = im.getFileImportId();
	    	/* file import kaydı atıldı*/
	    	
	    	int sheet_no = 1;
	    	for(Entry<String, List<HashMap<String, String>>> entry : parsedData.entrySet()){
	    		if(entry.getValue()!=null && entry.getValue().size()>0){
	    			/*excell shett kayıt ediliyor*/
	    			W5ExcelImportSheet ims = new W5ExcelImportSheet();
	            	ims.setProjectUuid(im.getProjectUuid());
	            	ims.setDsc(entry.getKey());
	            	ims.setTabOrder((short)sheet_no);
	            	ims.setExcelImportId(im.getExcelImportId());
	            	ims.setFileImportId(im.getFileImportId());
	            	ims.setCustomizationId(im.getCustomizationId());
	            	dao.saveObject(ims);
	    			/*excel sheet kaydedildi*/
		    		for(int i=0; i<parsedData.get(entry.getKey()).size(); i++){
		    			/* excel row row kaydediliyor.*/
		    			W5ExcelImportSheetData imd = new W5ExcelImportSheetData();
			    		imd.setRowNo(i+1);
			    		imd.setFileImportId(im.getFileImportId());
			    		imd.setExcelImportSheetId(ims.getExcelImportSheetId());
			    		imd.setSheetNo(sheet_no);
			    		imd.setProjectUuid(im.getProjectUuid());
			    		imd.setExcelImportId(im.getExcelImportId());
			    		imd.setImportFlag((short)0);
			    		imd.setCustomizationId(im.getCustomizationId());
			    		for(Entry<String, String> entryCols : parsedData.get(entry.getKey()).get(i).entrySet()){
			    			imd.setCell(entryCols.getKey(),entryCols.getValue());	
			    		}
			    		dao.saveObject(imd);
		    		}
	    		}
	    		sheet_no++;
	    	}
    	}	
		return file_import_id;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
