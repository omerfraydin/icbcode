FROM openjdk:8-jdk-alpine
COPY target/framework-0.0.1-SNAPSHOT.jar code2.jar
ENTRYPOINT ["java", "-jar", "/code2.jar"]